import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _f2f5c9ee = () => interopDefault(import('../pages/login.vue' /* webpackChunkName: "pages/login" */))
const _c88de56e = () => interopDefault(import('../pages/main.vue' /* webpackChunkName: "pages/main" */))
const _57df37a2 = () => interopDefault(import('../pages/test.vue' /* webpackChunkName: "pages/test" */))
const _32623416 = () => interopDefault(import('../pages/customer/form.vue' /* webpackChunkName: "pages/customer/form" */))
const _1ac80ccf = () => interopDefault(import('../pages/customer/list.vue' /* webpackChunkName: "pages/customer/list" */))
const _0b9451fa = () => interopDefault(import('../pages/customer/list-filter-paging.vue' /* webpackChunkName: "pages/customer/list-filter-paging" */))
const _a318ad0e = () => interopDefault(import('../pages/delivery/list.vue' /* webpackChunkName: "pages/delivery/list" */))
const _1aec8c68 = () => interopDefault(import('../pages/event/add.vue' /* webpackChunkName: "pages/event/add" */))
const _09a800fa = () => interopDefault(import('../pages/event/list.vue' /* webpackChunkName: "pages/event/list" */))
const _4b806288 = () => interopDefault(import('../pages/goods/add.vue' /* webpackChunkName: "pages/goods/add" */))
const _7deb2372 = () => interopDefault(import('../pages/goods/list.vue' /* webpackChunkName: "pages/goods/list" */))
const _c51ea44e = () => interopDefault(import('../pages/magazine/list.vue' /* webpackChunkName: "pages/magazine/list" */))
const _5b671793 = () => interopDefault(import('../pages/member/list.vue' /* webpackChunkName: "pages/member/list" */))
const _01aaf1ae = () => interopDefault(import('../pages/my-menu/logout.vue' /* webpackChunkName: "pages/my-menu/logout" */))
const _9c4e1dd6 = () => interopDefault(import('../pages/notice/list.vue' /* webpackChunkName: "pages/notice/list" */))
const _310a1562 = () => interopDefault(import('../pages/order/list.vue' /* webpackChunkName: "pages/order/list" */))
const _7d5edb97 = () => interopDefault(import('../pages/payment/list.vue' /* webpackChunkName: "pages/payment/list" */))
const _41c659c7 = () => interopDefault(import('../pages/question/list.vue' /* webpackChunkName: "pages/question/list" */))
const _199a2404 = () => interopDefault(import('../pages/delivery/detail/add.vue' /* webpackChunkName: "pages/delivery/detail/add" */))
const _5fb32a5e = () => interopDefault(import('../pages/magazine/detail/add.vue' /* webpackChunkName: "pages/magazine/detail/add" */))
const _2d93109a = () => interopDefault(import('../pages/notice/detail/add.vue' /* webpackChunkName: "pages/notice/detail/add" */))
const _3d84bd6d = () => interopDefault(import('../pages/customer/detail/_id.vue' /* webpackChunkName: "pages/customer/detail/_id" */))
const _14d5ec66 = () => interopDefault(import('../pages/customer/edit/_id.vue' /* webpackChunkName: "pages/customer/edit/_id" */))
const _dc229ad2 = () => interopDefault(import('../pages/delivery/detail/_id.vue' /* webpackChunkName: "pages/delivery/detail/_id" */))
const _44e64dbe = () => interopDefault(import('../pages/event/detail/_id.vue' /* webpackChunkName: "pages/event/detail/_id" */))
const _9ab3b636 = () => interopDefault(import('../pages/goods/detail/_id.vue' /* webpackChunkName: "pages/goods/detail/_id" */))
const _03222212 = () => interopDefault(import('../pages/magazine/detail/_id.vue' /* webpackChunkName: "pages/magazine/detail/_id" */))
const _6d0cb59e = () => interopDefault(import('../pages/member/detail/_id.vue' /* webpackChunkName: "pages/member/detail/_id" */))
const _6762559a = () => interopDefault(import('../pages/notice/detail/_id.vue' /* webpackChunkName: "pages/notice/detail/_id" */))
const _46a37065 = () => interopDefault(import('../pages/question/detail/_id.vue' /* webpackChunkName: "pages/question/detail/_id" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/login",
    component: _f2f5c9ee,
    name: "login"
  }, {
    path: "/main",
    component: _c88de56e,
    name: "main"
  }, {
    path: "/test",
    component: _57df37a2,
    name: "test"
  }, {
    path: "/customer/form",
    component: _32623416,
    name: "customer-form"
  }, {
    path: "/customer/list",
    component: _1ac80ccf,
    name: "customer-list"
  }, {
    path: "/customer/list-filter-paging",
    component: _0b9451fa,
    name: "customer-list-filter-paging"
  }, {
    path: "/delivery/list",
    component: _a318ad0e,
    name: "delivery-list"
  }, {
    path: "/event/add",
    component: _1aec8c68,
    name: "event-add"
  }, {
    path: "/event/list",
    component: _09a800fa,
    name: "event-list"
  }, {
    path: "/goods/add",
    component: _4b806288,
    name: "goods-add"
  }, {
    path: "/goods/list",
    component: _7deb2372,
    name: "goods-list"
  }, {
    path: "/magazine/list",
    component: _c51ea44e,
    name: "magazine-list"
  }, {
    path: "/member/list",
    component: _5b671793,
    name: "member-list"
  }, {
    path: "/my-menu/logout",
    component: _01aaf1ae,
    name: "my-menu-logout"
  }, {
    path: "/notice/list",
    component: _9c4e1dd6,
    name: "notice-list"
  }, {
    path: "/order/list",
    component: _310a1562,
    name: "order-list"
  }, {
    path: "/payment/list",
    component: _7d5edb97,
    name: "payment-list"
  }, {
    path: "/question/list",
    component: _41c659c7,
    name: "question-list"
  }, {
    path: "/delivery/detail/add",
    component: _199a2404,
    name: "delivery-detail-add"
  }, {
    path: "/magazine/detail/add",
    component: _5fb32a5e,
    name: "magazine-detail-add"
  }, {
    path: "/notice/detail/add",
    component: _2d93109a,
    name: "notice-detail-add"
  }, {
    path: "/customer/detail/:id?",
    component: _3d84bd6d,
    name: "customer-detail-id"
  }, {
    path: "/customer/edit/:id?",
    component: _14d5ec66,
    name: "customer-edit-id"
  }, {
    path: "/delivery/detail/:id?",
    component: _dc229ad2,
    name: "delivery-detail-id"
  }, {
    path: "/event/detail/:id?",
    component: _44e64dbe,
    name: "event-detail-id"
  }, {
    path: "/goods/detail/:id?",
    component: _9ab3b636,
    name: "goods-detail-id"
  }, {
    path: "/magazine/detail/:id?",
    component: _03222212,
    name: "magazine-detail-id"
  }, {
    path: "/member/detail/:id?",
    component: _6d0cb59e,
    name: "member-detail-id"
  }, {
    path: "/notice/detail/:id?",
    component: _6762559a,
    name: "notice-detail-id"
  }, {
    path: "/question/detail/:id?",
    component: _46a37065,
    name: "question-detail-id"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
