# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<LayoutAdminHeader>` | `<layout-admin-header>` (components/layout/admin-header.vue)
- `<LayoutAdminList>` | `<layout-admin-list>` (components/layout/admin-list.vue)
- `<LayoutAdminSidebarAvatar>` | `<layout-admin-sidebar-avatar>` (components/layout/admin-sidebar-avatar.vue)
- `<LayoutAdminSidebarMenu>` | `<layout-admin-sidebar-menu>` (components/layout/admin-sidebar-menu.vue)
- `<CommonLoadingIcon>` | `<common-loading-icon>` (components/common/loading-icon.vue)
