import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const VUEX_PROPERTIES = ['state', 'getters', 'actions', 'mutations']

let store = {};

(function updateModules () {
  store = normalizeRoot(require('../store/index.js'), 'store/index.js')

  // If store is an exported method = classic mode (deprecated)

  if (typeof store === 'function') {
    return console.warn('Classic mode for store/ is deprecated and will be removed in Nuxt 3.')
  }

  // Enforce store modules
  store.modules = store.modules || {}

  resolveStoreModules(require('../store/modules/test-data/index.js'), 'modules/test-data/index.js')
  resolveStoreModules(require('../store/modules/order/index.js'), 'modules/order/index.js')
  resolveStoreModules(require('../store/modules/notice/index.js'), 'modules/notice/index.js')
  resolveStoreModules(require('../store/modules/menu/index.js'), 'modules/menu/index.js')
  resolveStoreModules(require('../store/modules/member/index.js'), 'modules/member/index.js')
  resolveStoreModules(require('../store/modules/goods/index.js'), 'modules/goods/index.js')
  resolveStoreModules(require('../store/modules/delivery/index.js'), 'modules/delivery/index.js')
  resolveStoreModules(require('../store/modules/custom-loading/index.js'), 'modules/custom-loading/index.js')
  resolveStoreModules(require('../store/modules/authenticated/index.js'), 'modules/authenticated/index.js')
  resolveStoreModules(require('../store/modules/authenticated/actions.js'), 'modules/authenticated/actions.js')
  resolveStoreModules(require('../store/modules/authenticated/api-urls.js'), 'modules/authenticated/api-urls.js')
  resolveStoreModules(require('../store/modules/authenticated/constants.js'), 'modules/authenticated/constants.js')
  resolveStoreModules(require('../store/modules/authenticated/getters.js'), 'modules/authenticated/getters.js')
  resolveStoreModules(require('../store/modules/authenticated/mutations.js'), 'modules/authenticated/mutations.js')
  resolveStoreModules(require('../store/modules/authenticated/state.js'), 'modules/authenticated/state.js')
  resolveStoreModules(require('../store/modules/custom-loading/constants.js'), 'modules/custom-loading/constants.js')
  resolveStoreModules(require('../store/modules/custom-loading/getters.js'), 'modules/custom-loading/getters.js')
  resolveStoreModules(require('../store/modules/custom-loading/mutations.js'), 'modules/custom-loading/mutations.js')
  resolveStoreModules(require('../store/modules/custom-loading/state.js'), 'modules/custom-loading/state.js')
  resolveStoreModules(require('../store/modules/delivery/actions.js'), 'modules/delivery/actions.js')
  resolveStoreModules(require('../store/modules/delivery/api-urls.js'), 'modules/delivery/api-urls.js')
  resolveStoreModules(require('../store/modules/delivery/constants.js'), 'modules/delivery/constants.js')
  resolveStoreModules(require('../store/modules/delivery/getters.js'), 'modules/delivery/getters.js')
  resolveStoreModules(require('../store/modules/delivery/mutations.js'), 'modules/delivery/mutations.js')
  resolveStoreModules(require('../store/modules/delivery/state.js'), 'modules/delivery/state.js')
  resolveStoreModules(require('../store/modules/goods/actions.js'), 'modules/goods/actions.js')
  resolveStoreModules(require('../store/modules/goods/api-urls.js'), 'modules/goods/api-urls.js')
  resolveStoreModules(require('../store/modules/goods/constants.js'), 'modules/goods/constants.js')
  resolveStoreModules(require('../store/modules/goods/getters.js'), 'modules/goods/getters.js')
  resolveStoreModules(require('../store/modules/goods/mutations.js'), 'modules/goods/mutations.js')
  resolveStoreModules(require('../store/modules/goods/state.js'), 'modules/goods/state.js')
  resolveStoreModules(require('../store/modules/member/actions.js'), 'modules/member/actions.js')
  resolveStoreModules(require('../store/modules/member/api-urls.js'), 'modules/member/api-urls.js')
  resolveStoreModules(require('../store/modules/member/constants.js'), 'modules/member/constants.js')
  resolveStoreModules(require('../store/modules/member/getters.js'), 'modules/member/getters.js')
  resolveStoreModules(require('../store/modules/member/mutations.js'), 'modules/member/mutations.js')
  resolveStoreModules(require('../store/modules/member/state.js'), 'modules/member/state.js')
  resolveStoreModules(require('../store/modules/menu/constants.js'), 'modules/menu/constants.js')
  resolveStoreModules(require('../store/modules/menu/getters.js'), 'modules/menu/getters.js')
  resolveStoreModules(require('../store/modules/menu/mutations.js'), 'modules/menu/mutations.js')
  resolveStoreModules(require('../store/modules/menu/state.js'), 'modules/menu/state.js')
  resolveStoreModules(require('../store/modules/notice/actions.js'), 'modules/notice/actions.js')
  resolveStoreModules(require('../store/modules/notice/api-urls.js'), 'modules/notice/api-urls.js')
  resolveStoreModules(require('../store/modules/notice/constants.js'), 'modules/notice/constants.js')
  resolveStoreModules(require('../store/modules/notice/getters.js'), 'modules/notice/getters.js')
  resolveStoreModules(require('../store/modules/notice/mutations.js'), 'modules/notice/mutations.js')
  resolveStoreModules(require('../store/modules/notice/state.js'), 'modules/notice/state.js')
  resolveStoreModules(require('../store/modules/order/actions.js'), 'modules/order/actions.js')
  resolveStoreModules(require('../store/modules/order/api-urls.js'), 'modules/order/api-urls.js')
  resolveStoreModules(require('../store/modules/order/constants.js'), 'modules/order/constants.js')
  resolveStoreModules(require('../store/modules/order/getters.js'), 'modules/order/getters.js')
  resolveStoreModules(require('../store/modules/order/mutations.js'), 'modules/order/mutations.js')
  resolveStoreModules(require('../store/modules/order/state.js'), 'modules/order/state.js')
  resolveStoreModules(require('../store/modules/test-data/actions.js'), 'modules/test-data/actions.js')
  resolveStoreModules(require('../store/modules/test-data/api-urls.js'), 'modules/test-data/api-urls.js')
  resolveStoreModules(require('../store/modules/test-data/constants.js'), 'modules/test-data/constants.js')
  resolveStoreModules(require('../store/modules/test-data/getters.js'), 'modules/test-data/getters.js')
  resolveStoreModules(require('../store/modules/test-data/mutations.js'), 'modules/test-data/mutations.js')
  resolveStoreModules(require('../store/modules/test-data/state.js'), 'modules/test-data/state.js')

  // If the environment supports hot reloading...

  if (process.client && module.hot) {
    // Whenever any Vuex module is updated...
    module.hot.accept([
      '../store/index.js',
      '../store/modules/test-data/index.js',
      '../store/modules/order/index.js',
      '../store/modules/notice/index.js',
      '../store/modules/menu/index.js',
      '../store/modules/member/index.js',
      '../store/modules/goods/index.js',
      '../store/modules/delivery/index.js',
      '../store/modules/custom-loading/index.js',
      '../store/modules/authenticated/index.js',
      '../store/modules/authenticated/actions.js',
      '../store/modules/authenticated/api-urls.js',
      '../store/modules/authenticated/constants.js',
      '../store/modules/authenticated/getters.js',
      '../store/modules/authenticated/mutations.js',
      '../store/modules/authenticated/state.js',
      '../store/modules/custom-loading/constants.js',
      '../store/modules/custom-loading/getters.js',
      '../store/modules/custom-loading/mutations.js',
      '../store/modules/custom-loading/state.js',
      '../store/modules/delivery/actions.js',
      '../store/modules/delivery/api-urls.js',
      '../store/modules/delivery/constants.js',
      '../store/modules/delivery/getters.js',
      '../store/modules/delivery/mutations.js',
      '../store/modules/delivery/state.js',
      '../store/modules/goods/actions.js',
      '../store/modules/goods/api-urls.js',
      '../store/modules/goods/constants.js',
      '../store/modules/goods/getters.js',
      '../store/modules/goods/mutations.js',
      '../store/modules/goods/state.js',
      '../store/modules/member/actions.js',
      '../store/modules/member/api-urls.js',
      '../store/modules/member/constants.js',
      '../store/modules/member/getters.js',
      '../store/modules/member/mutations.js',
      '../store/modules/member/state.js',
      '../store/modules/menu/constants.js',
      '../store/modules/menu/getters.js',
      '../store/modules/menu/mutations.js',
      '../store/modules/menu/state.js',
      '../store/modules/notice/actions.js',
      '../store/modules/notice/api-urls.js',
      '../store/modules/notice/constants.js',
      '../store/modules/notice/getters.js',
      '../store/modules/notice/mutations.js',
      '../store/modules/notice/state.js',
      '../store/modules/order/actions.js',
      '../store/modules/order/api-urls.js',
      '../store/modules/order/constants.js',
      '../store/modules/order/getters.js',
      '../store/modules/order/mutations.js',
      '../store/modules/order/state.js',
      '../store/modules/test-data/actions.js',
      '../store/modules/test-data/api-urls.js',
      '../store/modules/test-data/constants.js',
      '../store/modules/test-data/getters.js',
      '../store/modules/test-data/mutations.js',
      '../store/modules/test-data/state.js',
    ], () => {
      // Update `root.modules` with the latest definitions.
      updateModules()
      // Trigger a hot update in the store.
      window.$nuxt.$store.hotUpdate(store)
    })
  }
})()

// createStore
export const createStore = store instanceof Function ? store : () => {
  return new Vuex.Store(Object.assign({
    strict: (process.env.NODE_ENV !== 'production')
  }, store))
}

function normalizeRoot (moduleData, filePath) {
  moduleData = moduleData.default || moduleData

  if (moduleData.commit) {
    throw new Error(`[nuxt] ${filePath} should export a method that returns a Vuex instance.`)
  }

  if (typeof moduleData !== 'function') {
    // Avoid TypeError: setting a property that has only a getter when overwriting top level keys
    moduleData = Object.assign({}, moduleData)
  }
  return normalizeModule(moduleData, filePath)
}

function normalizeModule (moduleData, filePath) {
  if (moduleData.state && typeof moduleData.state !== 'function') {
    console.warn(`'state' should be a method that returns an object in ${filePath}`)

    const state = Object.assign({}, moduleData.state)
    // Avoid TypeError: setting a property that has only a getter when overwriting top level keys
    moduleData = Object.assign({}, moduleData, { state: () => state })
  }
  return moduleData
}

function resolveStoreModules (moduleData, filename) {
  moduleData = moduleData.default || moduleData
  // Remove store src + extension (./foo/index.js -> foo/index)
  const namespace = filename.replace(/\.(js|mjs)$/, '')
  const namespaces = namespace.split('/')
  let moduleName = namespaces[namespaces.length - 1]
  const filePath = `store/${filename}`

  moduleData = moduleName === 'state'
    ? normalizeState(moduleData, filePath)
    : normalizeModule(moduleData, filePath)

  // If src is a known Vuex property
  if (VUEX_PROPERTIES.includes(moduleName)) {
    const property = moduleName
    const propertyStoreModule = getStoreModule(store, namespaces, { isProperty: true })

    // Replace state since it's a function
    mergeProperty(propertyStoreModule, moduleData, property)
    return
  }

  // If file is foo/index.js, it should be saved as foo
  const isIndexModule = (moduleName === 'index')
  if (isIndexModule) {
    namespaces.pop()
    moduleName = namespaces[namespaces.length - 1]
  }

  const storeModule = getStoreModule(store, namespaces)

  for (const property of VUEX_PROPERTIES) {
    mergeProperty(storeModule, moduleData[property], property)
  }

  if (moduleData.namespaced === false) {
    delete storeModule.namespaced
  }
}

function normalizeState (moduleData, filePath) {
  if (typeof moduleData !== 'function') {
    console.warn(`${filePath} should export a method that returns an object`)
    const state = Object.assign({}, moduleData)
    return () => state
  }
  return normalizeModule(moduleData, filePath)
}

function getStoreModule (storeModule, namespaces, { isProperty = false } = {}) {
  // If ./mutations.js
  if (!namespaces.length || (isProperty && namespaces.length === 1)) {
    return storeModule
  }

  const namespace = namespaces.shift()

  storeModule.modules[namespace] = storeModule.modules[namespace] || {}
  storeModule.modules[namespace].namespaced = true
  storeModule.modules[namespace].modules = storeModule.modules[namespace].modules || {}

  return getStoreModule(storeModule.modules[namespace], namespaces, { isProperty })
}

function mergeProperty (storeModule, moduleData, property) {
  if (!moduleData) {
    return
  }

  if (property === 'state') {
    storeModule.state = moduleData || storeModule.state
  } else {
    storeModule[property] = Object.assign({}, storeModule[property], moduleData)
  }
}
