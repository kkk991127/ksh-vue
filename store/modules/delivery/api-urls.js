const BASE_URL = '/v1/delivery'

export default {
    //리스트
    DO_DELIVERY_LIST: `${BASE_URL}/all`, //get
    //단수
    DO_DELIVERY_DETAIL: `${BASE_URL}/{id}`, //get

    DO_DELIVERY_CREATE: `${BASE_URL}/new`, //post

}
