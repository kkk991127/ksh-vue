export default {



    DO_DELIVERY_LIST: '/delivery/list',
    DO_LIST_PAGING: 'test-data/doListPaging',
    DO_DELIVERY_DETAIL: '/delivery/detail/id',
    DO_CREATE_COMMENT: 'test-data/doCreateComment',
    DO_DELIVERY_UPDATE: 'test-data/doUpdate',
    DO_DELETE: 'test-data/doDelete',
    DO_DELIVERY_CREATE: '/delivery/form',


}
