const BASE_URL = 'http://34.122.41.137:8080/v1/order'

export default {
    DO_LIST: `${BASE_URL}/all`, //get
    DO_LIST_PAGING: `${BASE_URL}/page/{pageNum}`, //get
    DO_DETAIL: `${BASE_URL}/{id}`, //get
    DO_CREATE_COMMENT: `${BASE_URL}/comment/document-id/{id}`, //post
    DO_UPDATE: `${BASE_URL}/{id}`, //put
    DO_DELETE: `${BASE_URL}/{id}`, //del
    DO_CREATE: `${BASE_URL}/new`, //post
}
