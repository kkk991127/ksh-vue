const BASE_URL =  '/v1/member'

export default {
    DO_MEMBER_LIST: `${BASE_URL}/all`, //get

    DO_MEMBER_DETAIL: `${BASE_URL}/detail/{id}`, //get


}
